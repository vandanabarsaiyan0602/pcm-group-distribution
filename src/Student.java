public class Student implements Comparable<Student>{
    private String name;
    private int physicsScore;
    private int chemistryScore;
    private int mathsScore;
    private int csScore;
    public Student(String name,int physicsScore,int chemistryScore,int mathsScore,int csScore){
        this.name=name;
        this.chemistryScore=chemistryScore;
        this.physicsScore=physicsScore;
        this.mathsScore=mathsScore;
        this.csScore=csScore;
    }
    public double calculatePCMPercentage(){
        return (chemistryScore+mathsScore+physicsScore)*100/300.0;
    }
    public int getCsScore(){
        return this.csScore;
    }
    public String getName() {
        return name;
    }
    public double getPercentage(){
        return (chemistryScore+mathsScore+physicsScore+csScore)*100/400.0;
    }
    public int compareTo(Student obj){
        if(this.getPercentage()>obj.getPercentage())
            return -1;
        else
            return 1;
    }

}
