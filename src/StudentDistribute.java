import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class StudentDistribute {
    public static void main(String[] args) {
        ArrayList<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Amir", 67, 57, 80, 80));
        studentList.add(new Student("Shweta", 80, 89, 70, 60));
        studentList.add(new Student("Jay", 78, 90, 80, 84));
        studentList.add(new Student("Kushal", 65, 77, 90, 89));
        studentList.add(new Student("Raj", 89, 70, 70, 56));
        studentList.add(new Student("John", 87, 67, 90, 58));
        studentList.add(new Student("Mick", 92, 82, 98, 90));
        distribute(studentList);
        findRank(studentList);
    }
    public static void distribute(ArrayList<Student> studentList){
        HashMap<String,ArrayList<String>> hm=new HashMap<>();
        for(Student s:studentList){
            double pcm=s.calculatePCMPercentage();
            int csScore=s.getCsScore();
            String studentName=s.getName();
            if(pcm>=70 && csScore>=80){
                ArrayList<String> list=hm.get("Computer Science");
                if(list==null) {
                    list=new ArrayList<>();
                }
                list.add(studentName);
                hm.put("Computer Science", list);
            }
            else if(pcm>70){
                ArrayList<String> list=hm.get("Biology");
                if(list==null) {
                    list=new ArrayList<>();
                }
                list.add(studentName);
                hm.put("Biology", list);
            }
            else {
                ArrayList<String> list=hm.get("Commerce");
                if(list==null) {
                    list=new ArrayList<>();
                }
                list.add(studentName);
                hm.put("Commerce", list);
            }
        }
        System.out.println("Computer Science:"+hm.get("Computer Science"));
        System.out.println("Biology:"+hm.get("Biology"));
        System.out.println("Commerce:"+hm.get("Commerce"));
    }
    public static void findRank(ArrayList<Student> studentList){
        Collections.sort(studentList);
        int i=1;
        for(Student s:studentList){
            System.out.println("Rank #"+i+":"+ s.getName()+" %:"+s.getPercentage());
            i++;
        }
    }
}